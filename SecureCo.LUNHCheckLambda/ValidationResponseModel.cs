﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureCo.LUNHCheckLambda
{
    public class ValidationResponseModel
    {
        public bool IsValid { get; internal set; }
        public string ValidationMessage { get; internal set; }
    }
}
