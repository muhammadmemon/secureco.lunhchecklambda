﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureCo.LUNHCheckLambda
{
    public class LUNHCheck
    {
        public static ValidationResponseModel ValidateLUNH(string pan)
        {
            ValidationResponseModel responseModel = new ValidationResponseModel
            {
                IsValid = false,
                ValidationMessage = ""
            };

            // Check whether pan is null or empty
            if (string.IsNullOrEmpty(pan))
            {
                responseModel.IsValid = false;
                responseModel.ValidationMessage = "PAN can NOT be empty.";
                return responseModel;
            }

            /* 
             * 1. Starting with the check digit double the value of every other digit 
             * 2. If doubling of a number results in a two digits number, add up
             *     the digits to get a single digit number. This will results in eight single digit numbers                    
             * 3. Get the sum of the digits
             */
            int sumOfDigits = pan.Where((e) => e >= '0' && e <= '9')
                .Reverse()
                .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2))
                .Sum((e) => e / 10 + e % 10);


            // If the final sum is divisible by 10, then the credit card number
            //   is valid. If it is not divisible by 10, the number is invalid. 
            if (sumOfDigits % 10 == 0)
            {
                responseModel.IsValid = true;
                responseModel.ValidationMessage = "";
                return responseModel;
            }

            // If we made it here then then pan failed the LUNH check.
            responseModel.IsValid = false;
            responseModel.ValidationMessage = "PAN is invalid.";

            return responseModel;

        }
    }
}
